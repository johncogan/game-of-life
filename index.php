<?php
  /**
   * PHP system based upon Conways game of life, explained at https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
   *
   * Author: John Cogan
   * Date: 18/12/2015
   * Time: 13:44
   *
   * To run this, use terminal (Expand terminal size a bit so the lines do not wrap.
   * Command: php index.php
   */

  $canvasSize = array(100,100);
  $canvas[][] = null;
  $canvasScratch[][] = null;

  define('CHAR_ALIVE', 'O');
  define('CHAR_DEAD', '-');

  // Prepare the scratch canvas and current canvas
  prepareCanvas($canvas, $canvasSize[0], $canvasSize[1], CHAR_DEAD);
  prepareCanvas($canvasScratch, $canvasSize[0], $canvasSize[1], CHAR_DEAD);

  // Set up the initial conditions/values, also known as the 'seed'
  // Play around with position of seed values to get different patterns
  $do1 = true;

  if($do1) {
    // 2 Blinker's - with a period of 2
    updatePosition($canvas, 80, 25, CHAR_ALIVE);
    updatePosition($canvas, 80, 26, CHAR_ALIVE);
    updatePosition($canvas, 81, 25, CHAR_ALIVE);
    updatePosition($canvas, 81, 26, CHAR_ALIVE);
    updatePosition($canvas, 83, 22, CHAR_ALIVE);
    updatePosition($canvas, 83, 23, CHAR_ALIVE);
    updatePosition($canvas, 84, 22, CHAR_ALIVE);
    updatePosition($canvas, 84, 23, CHAR_ALIVE);

    // Glider - Will collide with the two blinkers above
    // and then stabilize
    updatePosition($canvas, 90, 10, CHAR_ALIVE);
    updatePosition($canvas, 90, 11, CHAR_ALIVE);
    updatePosition($canvas, 90, 12, CHAR_ALIVE);
    updatePosition($canvas, 91, 12, CHAR_ALIVE);
    updatePosition($canvas, 92, 11, CHAR_ALIVE);

    // Glider - Will collide with the two blinkers above
    updatePosition($canvas, 90, 16, CHAR_ALIVE);
    updatePosition($canvas, 90, 17, CHAR_ALIVE);
    updatePosition($canvas, 90, 18, CHAR_ALIVE);
    updatePosition($canvas, 91, 18, CHAR_ALIVE);
    updatePosition($canvas, 92, 17, CHAR_ALIVE);

    /*
     Starts as:

    ---------------------------------
    -------------------------OO------
    -------------------------OO------
    ---------------------------------
    ----------------------OO---------
    ----------------------OO---------
    ---------------------------------
    ---------------------------------
    ---------------------------------
    ---------------------------------
    -----------O-----O---<- Gliders
    -----------OO----OO--------------
    ----------O-O---O-O------------


    // Stabilizes into form:

              --------O------------
---------------------O-O-----------
---------------------O-O-----------
----------------------O------------
-----------------------------------
-----------------OO-------OO-------
----------------O--O-----O--O------
-----------------OO-------OO-------
-----------------------------------
----------------------O------------
---------------------O-O-----------
---------------------O-O-----------
----------------------O------------
     */
  }


  // Run the simulation for a specified time
  for($i = 0; $i < 200; $i++) {
    tick($canvas, $canvasScratch);
    $canvas = $canvasScratch;
    prepareCanvas($canvasScratch, $canvasSize[0], $canvasSize[1], CHAR_DEAD);

    drawCanvas($canvas);
  }

  /**
   * @param $currentCanvas
   * @param $scratchCanvas
   */
  function tick(&$currentCanvas, &$scratchCanvas){
    $canvasWidth = sizeOf($currentCanvas[0]);
    $canvasHeight = sizeOf($currentCanvas);

    for ($x = 0; $x < $canvasWidth; $x++) {
      for ($y = 0; $y < $canvasHeight; $y++) {
        $currentCellValue = $currentCanvas[$x][$y];
        $bitCount = countSurroundingPopulatedBits($currentCanvas, $x, $y);
        $deadOrAlive = calculateDeadOrAlive($currentCellValue, $bitCount);
        updatePosition($scratchCanvas, $x, $y, $deadOrAlive);
      }
    }
  }

  /**
   * @param $currentCellValue
   * @param $aliveBits
   * @return string
   */
  function calculateDeadOrAlive($currentCellValue, $aliveBits){
    $rule_x = true; // rule_x is standard Game of Life rule

    if($rule_x) {
      if ($currentCellValue == CHAR_ALIVE) {
        // Live cell rules
        switch ($aliveBits) {
          case 0:
            return CHAR_DEAD;
            break;
          case 1:
            return CHAR_DEAD;
            break;
          case 2:
            return CHAR_ALIVE;
            break;
          case 3:
            return CHAR_ALIVE;
            break;
          default:
            return CHAR_DEAD;
            break;
        }
      }
      else {
        // Dead cell rules
        switch ($aliveBits) {
          case 3:
            return CHAR_ALIVE;
            break;
          default:
            return CHAR_DEAD;
            break;
        }
      }
    }
    echo 'Rule error';
    die();
  }

  /**
   * @param $canvas
   * @param $positionRow
   * @param $positionCol
   * @return int
   */
  function countSurroundingPopulatedBits($canvas, $positionRow, $positionCol){

    $totalAliveBits = 0;

    for($counterCol = -1; $counterCol < 2; $counterCol++){
      for($counterRow = -1; $counterRow < 2; $counterRow++){

        $isCenterElement = ($counterRow==0 && $counterCol==0)?TRUE:FALSE;

        // Calculate which position we are going to look at
        $posRow = $positionRow + $counterRow;
        $posCol = $positionCol + $counterCol;

        // Work out if we are outside out canvas area
        $isRowOutOfBounds = (($posRow<0) || ($posRow>=sizeOf($canvas)))?TRUE:FALSE;
        $isColOutOfBounds = (($posCol<0) || ($posCol>=sizeOf($canvas[0])))?TRUE:FALSE;

        // If either the column or row is outside of the canvas we want to flag indicating this.
        $outOfBounds = ($isRowOutOfBounds || $isColOutOfBounds)?TRUE:FALSE;

        // If we are out of bounds we treat this a a 'dead' bit, so we only operate when in-bounds
        if(!$outOfBounds){
          // Only run calculation when we are NOT the center bit
          if(!$isCenterElement){

            $valOfTargetElement = $canvas[$posRow][$posCol];

            if($valOfTargetElement == CHAR_ALIVE){
              $totalAliveBits++;
            }
          }
        }

      }
    }

    return $totalAliveBits;

  }

  /**
   * @param $canvas
   * @param $positionRow
   * @param $positionCol
   * @param $value
   */
  function updatePosition(&$canvas, $positionRow, $positionCol, $value){
    $canvas[$positionRow][$positionCol] = $value;
  }

  /**
   * @param $canvasRef
   */
  function drawCanvas($canvasRef){
    exec('clear');  // Used to clear the console for the next tick
    $canvasWidth = sizeof($canvasRef);
    $canvasHeight= sizeof($canvasRef[0]);

    for ($x = 0; $x < $canvasWidth; $x++) {
      for ($y = 0; $y < $canvasHeight; $y++) {
        echo $canvasRef[$x][$y];
      }
      echo PHP_EOL;
    }
  }

  /**
   * @param $canvasRef
   * @param $canvasWidth
   * @param $canvasHeight
   * @param $cellValue
   */
  function prepareCanvas(&$canvasRef, $canvasWidth, $canvasHeight, $cellValue) {
    for ($x = 0; $x < $canvasWidth; $x++) {
      for ($y = 0; $y < $canvasHeight; $y++) {
        $canvasRef[$x][$y] = $cellValue;
      }
    }
  }
